﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Speech;
using System.Speech.Recognition;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Gutachter_Samples.View.PageChanger
{
    /// <summary>
    /// Interaktionslogik für SprachNotizPage.xaml
    /// </summary>
    public partial class SprachNotizPage : Page
    {
        public SprachNotizPage()
        {
            InitializeComponent();
            SpeechRecognizer speechRecognizer = new SpeechRecognizer();
        }

        private void btnAufnahmeClick(object sender, RoutedEventArgs e)
        {
            SpeechRecognizer spRecognizer = new SpeechRecognizer();

            spRecognizer.Enabled = true;

            spRecognizer.SpeechRecognized += new EventHandler<SpeechRecognizedEventArgs>(spRecognizer_SpeechRecognized);

        }

        void spRecognizer_SpeechRecognized(object sender, SpeechRecognizedEventArgs e)

        {
            string str = e.Result.Text;
            txtSpeech.Text = e.Result.Text.ToString();
        }
    }
}
