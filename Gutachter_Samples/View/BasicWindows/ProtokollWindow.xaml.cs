﻿using Gutachter_Samples.View.PageChanger;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Gutachter_Samples.View
{
    /// <summary>
    /// Interaktionslogik für ProtokollWindow.xaml
    /// </summary>
    public partial class ProtokollWindow : Window
    { 
        public ProtokollWindow()
        {
            InitializeComponent();
        }

        private void btnPageClickStart(object sender, RoutedEventArgs e)
        {
            PageWindow.Content = new StartPage();
        }

        private void btnPageSchadenAufnehmen(object sender, RoutedEventArgs e)
        {
            PageWindow.Content = new SchadenAufnahmePage();
        }

        private void btnPageBilder(object sender, RoutedEventArgs e)
        {
            PageWindow.Content = new BildAufnehmenPage();

        }

        private void btnPageSprachNotiz(object sender, RoutedEventArgs e)
        {
            PageWindow.Content = new SprachNotizPage();

        }
    }
}
