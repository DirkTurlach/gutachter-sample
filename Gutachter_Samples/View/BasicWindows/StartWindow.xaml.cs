﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Gutachter_Samples.View.PageChanger;

namespace Gutachter_Samples.View
{
    /// <summary>
    /// Interaktionslogik für Startfenster.xaml
    /// </summary>
    public partial class StartWindow : Window
    {
        public StartWindow()
        {
            InitializeComponent();
        }

        private void btnSchadenAufnehmen(object sender, RoutedEventArgs e)
        {
            ProtokollWindow frm = new ProtokollWindow();
            frm.Show();
        }
    }
}
